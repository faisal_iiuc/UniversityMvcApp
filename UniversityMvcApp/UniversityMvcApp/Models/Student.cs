﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace UniversityMvcApp.Models
{
    public class Student
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter Student Name")]


        public string Name { get; set; }
        [Required(ErrorMessage = "Please Enter Student Email")]
        [RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", ErrorMessage = "Please Enter Valid Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please Enter Student Contact No")]
        [DisplayName("Contact No")]
        public string ContactNo { get; set; }

        [Required(ErrorMessage = "Please Enter Date")]

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }


        [Required(ErrorMessage = "Please Enter  Address")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Please Enter  DepartmentId")]
        [DisplayName("DepartmentId")]
        
        public int DepartmentId { get; set; }
        [ForeignKey("DepartmentId")]
        public virtual Department Department { get; set; }

        public string RegistrationSerial { get; set; }
    }
}