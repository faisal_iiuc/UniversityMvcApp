﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace UniversityMvcApp.Models
{
    public class UniversityDbContext:DbContext
    {
       public DbSet<Course> Courses { get; set; }
       public DbSet<Department> Departments { get; set; }
       public DbSet<Teacher> Teachers { get; set; }
       public DbSet<Student> Students { get; set; }
       public DbSet<Semester> Semesters { get; set; }

       public DbSet<Designation> Designations { get; set; }
       public DbSet<Room> Rooms { get; set; }
       public DbSet<AllocateClassRoom> AllocateClassRooms { get; set; }
       public DbSet<EnrollCourses> EnrollCourses { get; set; }


    }
}