﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityMvcApp.Models
{
    public class EnrollCourses
    {
        public int Id { get; set; }
        public int CourseId { get; set; }
        public Course course { get; set; }

        public int StudentId { get; set; }

        public Student Student { get; set; }

        public string Grade { get; set; }

        public DateTime date { get; set; }
    }
}