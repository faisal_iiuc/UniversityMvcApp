﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using UniversityMvcApp.Models;

namespace UniversityMvcApp.DAL.Gateway
{
    public class ViewClassScheduleWithRoomAllocationsGateway
    {
        private string connectionString =
          WebConfigurationManager.ConnectionStrings["UniversityDbContext"].ConnectionString;

        public ViewClassScheduleWithRoomAllocation GetView(Course course)
        {
            ViewClassScheduleWithRoomAllocation viewClassScheduleWithRoomAllocations = new ViewClassScheduleWithRoomAllocation();
            SqlConnection connection = new SqlConnection(connectionString);
            string Query = "SELECT * FROM AllocateClassRooms JOIN Rooms ON AllocateClassRooms.RoomId=Rooms.Id WHERE CourseId='" + course.Id + "'";
            SqlCommand command = new SqlCommand(Query, connection);
            viewClassScheduleWithRoomAllocations.CourseCode = course.Code;
            viewClassScheduleWithRoomAllocations.CourseName = course.Name;
            viewClassScheduleWithRoomAllocations.ScheduleInfo = "";
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if(!reader.HasRows)
            {
                viewClassScheduleWithRoomAllocations.ScheduleInfo = "Not Scheduled Yet!";
            }
            else
            {
                while (reader.Read())
                {
                    viewClassScheduleWithRoomAllocations.ScheduleInfo += "RooM No: " + reader["RoomNo"].ToString();
                    viewClassScheduleWithRoomAllocations.ScheduleInfo += ", "+reader["Day"].ToString();
                    viewClassScheduleWithRoomAllocations.ScheduleInfo += ", " + reader["From"].ToString();
                    viewClassScheduleWithRoomAllocations.ScheduleInfo += "- " + reader["TO"].ToString();
                    viewClassScheduleWithRoomAllocations.ScheduleInfo += "\n ";


                }
           
            }
            reader.Close();
            connection.Close();


            return viewClassScheduleWithRoomAllocations;
        }
    }
}