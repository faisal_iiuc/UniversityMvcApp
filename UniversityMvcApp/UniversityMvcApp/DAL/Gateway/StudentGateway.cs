﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace UniversityMvcApp.DAL.Gateway
{
    public class StudentGateway
    {
        public string ConnectionString = WebConfigurationManager.ConnectionStrings["UniversityDBContext"].ConnectionString;


        public string GetRegistrationNumber(Models.Student student)
        {
            int count = 0;
            SqlConnection connection = new SqlConnection(ConnectionString);

            string Query = "SELECT (COUNT(1)+1) FROM Students WHERE DepartmentId='"+student.DepartmentId+"' and DATEPART(YEAR,Date)='"+student.Date.Year+"'";

            SqlCommand command = new SqlCommand(Query, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                count = (int)reader[0];
            }
            connection.Close();

            string Query2 = "SELECT * FROM Departments WHERE ID='" + student.DepartmentId + "' ";

            SqlCommand command2 = new SqlCommand(Query2, connection);
            connection.Open();
            SqlDataReader reader2 = command2.ExecuteReader();
            string DeptCode="";
            while (reader2.Read())
            {
                DeptCode = reader2["Code"].ToString();
            }
            connection.Close();

            string registration = DeptCode + "-" + student.Date.Year + "-" + count.ToString("000");


            return registration;

        }
    }
}