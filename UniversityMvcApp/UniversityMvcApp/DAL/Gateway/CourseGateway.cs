﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using UniversityMvcApp.Models;

namespace UniversityMvcApp.DAL.Gateway
{
    public class CourseGateway
    {
        private bool message = false;
        private string connectionString =
            WebConfigurationManager.ConnectionStrings["UniversityDbContext"].ConnectionString;
        public bool IsCodeExist(string code)
        {
            SqlConnection connection=new SqlConnection(connectionString);
            string Query = "SELECT * FROM Courses Where Code='" + code + "' ";
            SqlCommand command=new SqlCommand(Query,connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                message= true;
            }
            reader.Close();
            connection.Close();


            return message;

        }

        public bool IsNameExist(string name)
        {
             
            SqlConnection connection = new SqlConnection(connectionString);
            string Query = "SELECT * FROM Courses Where Name='" + name + "' ";
            SqlCommand command = new SqlCommand(Query, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                message = true;
            }
            reader.Close();
            connection.Close();

            return message;
        }
        public List<Course> GetAllCourses()
        {
            SqlConnection connection = new SqlConnection(connectionString);
            string Query = "SELECT * FROM Courses ";
            SqlCommand command = new SqlCommand(Query, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            List<Course> courses = new List<Course>();
            while (reader.Read())
            {
                Course course = new Course()
                {
                    Id = (int)reader["Id"],
                    Name = reader["Name"].ToString(),
                    Code = reader["Code"].ToString(),
                    DepartmentId = (int)reader["DepartmentId"],
                    Credit=(decimal)reader["Credit"],
                    SemesterId=(int)reader["SemesterId"],
                    TeacherId =reader["TeacherId"] as int? ?? default(int)
                };
                courses.Add(course);
            }
            reader.Close();
            connection.Close();

            return courses;
        }

        public bool IsCourseAssign(int courseId)
        {
            
            SqlConnection connection = new SqlConnection(connectionString);
            string Query = "SELECT * FROM Courses Where Id='" + courseId + "' ";
            SqlCommand command = new SqlCommand(Query, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            while(reader.Read())
            {
                if((reader["TeacherId"] as int? )!=null)
                {
                    message = true;
                }

           
                
            }
            reader.Close();
            connection.Close();

            return message;
        }



        public bool AssignTeacher(Course Courses)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            string Query = "UPDATE Courses SET TeacherId='"+Courses.TeacherId+"' WHERE Id='"+Courses.Id+"'   ";
            SqlCommand command = new SqlCommand(Query, connection);
            connection.Open();
            int roweff = command.ExecuteNonQuery();
            if (roweff!=0)
            {
                message = true;
            }
           
            connection.Close();

            return message;
        }


       
    }
}