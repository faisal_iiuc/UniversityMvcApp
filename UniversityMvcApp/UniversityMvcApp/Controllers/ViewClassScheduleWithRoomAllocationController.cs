﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityMvcApp.BLL;
using UniversityMvcApp.Models;

namespace UniversityMvcApp.Controllers
{
    public class ViewClassScheduleWithRoomAllocationController : Controller
    {
        ViewClassScheduleWithRoomAllocationsManager viewClassScheduleWithRoomAllocationsManager = new ViewClassScheduleWithRoomAllocationsManager();
        DepartmentManager departmentManager = new DepartmentManager();
        CourseManager aCourseManager = new CourseManager();
        // GET: ViewClassScheduleWithRoomAllocation


        public ActionResult ViewCourseDetails()
        {
            ViewBag.departments = departmentManager.GetDepartments();

            return View();
        }
        [HttpPost]
        public ActionResult ViewCourseDetails(Department department)
        {
            ViewBag.departments = departmentManager.GetDepartments();

            return View();
        }
        public JsonResult GetCourseTableByDepartment(int departmentId)
        {
            //List<Course> course = db.Courses.ToList();

            List<Course> courses = aCourseManager.GetAllCourses();
            var course = courses.Where(a => a.DepartmentId == departmentId).ToList();
            List<ViewClassScheduleWithRoomAllocation> viewClassScheduleWithRoomAllocations = new List<ViewClassScheduleWithRoomAllocation>();
            foreach (var course1 in course)
            {
                viewClassScheduleWithRoomAllocations.Add(viewClassScheduleWithRoomAllocationsManager.GetView(course1));
            }

            return Json(viewClassScheduleWithRoomAllocations);
            //return Json(courses, JsonRequestBehavior.AllowGet);
        }
    }
}