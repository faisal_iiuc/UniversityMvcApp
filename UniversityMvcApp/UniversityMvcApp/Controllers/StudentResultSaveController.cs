﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityMvcApp.BLL;
using UniversityMvcApp.DAL.Gateway;
using UniversityMvcApp.Models;

namespace UniversityMvcApp.Controllers
{
    public class StudentResultSaveController : Controller
    {
        private UniversityDbContext db = new UniversityDbContext();
        EnrollCourseManager enrollCourseManager = new EnrollCourseManager();
        // GET: EnrollCourses
        public ActionResult Save()
        {
            ViewBag.StudentId = db.Students.ToList();
            return View();
        }
        [HttpPost]
        public ActionResult Save(EnrollCourses enrollCourse)
        {
            ViewBag.StudentId = db.Students.ToList();


            if (ModelState.IsValid)
            {
                //db.EnrollCourses.SqlQuery("Update EnrollCourses SET Grade='"+enrollCourse.Grade+"' Where CourseId='"+enrollCourse.CourseId+"'");
                if (enrollCourseManager.UpdateEnrollCourse(enrollCourse))
                {
                    ViewBag.saveMessage = "Student Result Add Successfully!";
                }
                else
                {
                    ViewBag.saveMessage = "Student Result Not Save!";
                }


            }

            return View();
        }



        public JsonResult GetCourseByStudentId(int studentId)
        {
            //List<Course> course = db.Courses.ToList();


            //var students = db.Students.ToList();
            //var student = students.Find(a => a.Id == studentId);
            //List<Course> courses = db.Courses.ToList();

            List<EnrollCourses> enrollCourses = enrollCourseManager.GetEnrollCourseByDepartment(studentId);
            List<Course> course = new List<Course>();
            foreach (var enrollCourse in enrollCourses)
            {
                course.Add(enrollCourseManager.GetCourseByStudent(enrollCourse.CourseId));
            }


            //return Json(course);
            return Json(course, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetStudentByStudentId(int studentId)
        {
            //List<Course> course = db.Courses.ToList();


            //var students = db.Students.ToList();
            //var student = students.Find(a => a.Id == studentId);
            //List<Course> courses = db.Courses.ToList();
            Student student = enrollCourseManager.SearchStudentById(studentId);
            string Name = student.Name;
            //return Json(course);
            return Json(Name, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDepartmentNameByStudentId(int studentId)
        {
            Student student = enrollCourseManager.SearchStudentById(studentId);
            Department Department = enrollCourseManager.SearchDepartmentStudentById(student.DepartmentId);
            string Name = Department.Name;
            //return Json(course);
            return Json(Name, JsonRequestBehavior.AllowGet);
        }

    }
}